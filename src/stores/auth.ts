import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
  const currentUser = ref<User>({
      id: 1,
      email: 'Nagi123@gmail.com',
      password: 'Pass123',
      fullName: 'Nagi Seishiro',
      gender: 'male',
      roles: ['user']
  })

  return { currentUser }
})
